extends Node2D

signal detail_light
signal detail_texture
signal detail_physics
signal detail_particle

func load_options():
	$UI/Lights.select(Globals.Configuration.LightDetail)
	$UI/Physics.select(Globals.Configuration.PhysicsDetail)
	$UI/Textures.select(Globals.Configuration.TextureDetail)
	$UI/Particles.select(Globals.Configuration.ParticleDetail)

func _ready():
	load_options()
	Globals.update_graphics()

func _on_Particles_item_selected(option_id):
	Globals.set_configuration('ParticleDetail', option_id)
	$Candle.select_texture()

func _on_Lights_item_selected(option_id):
	Globals.set_configuration('LightDetail', option_id)
	$Candle.set_shadow_enabled(option_id > 0)

func _on_Textures_item_selected(option_id):
	Globals.set_configuration('TextureDetail', option_id)
	$Candle.select_texture()

func _on_Physics_item_selected(option_id):
	Globals.set_configuration('PhysicsDetail', option_id)
