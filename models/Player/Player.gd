extends "res://models/Player/Character.gd"

const WALL_JUMP_BOOST = 5

var Camera = null
var WallJump = null

func setup_camera():
	## First create an instance of our already created camera
	var camera = load("res://models/Player/Camera2D.tscn").instance()

	## Attach it to the Model, the kinematic body will now move the camera around with it
	self.Model.add_child(camera)

	## Cleanup any already allocated resources
	if self.Camera:
		self.Camera.queue_free()

	## We might need this later
	self.Camera = camera

func setup_wall_jump():
	## Create and add the wall jump detection area
	## We'd like to wall jump when the player is very close to the wall
	## But perhaps not necesarily touching the wall
	var wall_jump = load("res://models/Player/WallJump.tscn").instance()
	self.Model.add_child(wall_jump)

	## Cleanup any already allocated resources
	if self.WallJump:
		self.WallJump.queue_free()

	## We might need this later
	self.WallJump = wall_jump

func set_model(model):
	## First call our inherited method
	.set_model(model);

	## Then add player specific nodes
	self.setup_camera()
	self.setup_wall_jump()

func _ready():
	setup_camera()

func wall_jump_collision():
	var collision_bodies = self.WallJump.get_overlapping_bodies()
	if not len(collision_bodies): return Vector2(0, 0)

	var first_body = collision_bodies[0]
	if first_body.position.x > self.Model.position.x:
		## Player will jump to the left
		return Vector2(-1, 0)

	## Player will jump to the right
	return Vector2(1, 0)

func add_wall_jump(motion):
	var collision_vector = wall_jump_collision()

	## Simple way to preserve our jump direction
	var direction_x = -1 if collision_vector.x < 0 else 1
	var direction_y = -1 if collision_vector.y < 0 else 1

	## Calculating the wall jump velocity
	var velocity_x = clamp(abs(collision_vector.x), 0.2, 1) * JUMP_SPEED * 4
	var velocity_y = clamp(abs(collision_vector.y), 0.4, 1) * -JUMP_SPEED * WALL_JUMP_BOOST

	## Final wall jump is velocity * direction
	return motion + Vector2(velocity_x * direction_x, velocity_y * direction_y)

func can_wall_jump():
	if self.Model.is_on_floor(): return false
	return len(self.WallJump.get_overlapping_bodies()) > 0

func add_jumping(motion):
	if not can_jump(motion): return motion
	if not Input.is_action_pressed("ui_up"): return motion

	## We're close enough to a wall to perform a wall jump
	if can_wall_jump():
		return add_wall_jump(motion)

	return motion + Vector2(0, -JUMP_SPEED)

func add_movement(motion):
	if Input.is_action_pressed("ui_right"):
		return motion + Vector2(MOVE_SPEED, 0)

	if Input.is_action_pressed("ui_left"):
		return motion - Vector2(MOVE_SPEED, 0)

	return motion

func _physics_process(delta):
	"""
	Physics process is called once per physics frame
	and is the correct place to put any and all physics processing.

	Here we'll slowly build up our idea of what the characters
	next velocity should be, based on input and environmental factors.

	Finally we'll `move_and_slide` to determine the next velocity,
	and factor collisions automatically (godot)
	"""
	var motion = self.Velocity;

	## Add player input if we're still alive
	if self.is_alive():
		motion = add_jumping(motion)
		motion = add_movement(motion)

	## Always apply environmental factors
	motion = add_friction(motion)
	motion = add_gravity(motion)

	## Limit maxium speed in any direction
	motion = clamp_movement(motion)

	self.Velocity = self.Model.move_and_slide(
		motion,

		## Specify our UP vector
		## This helps with is_on_floor calls
		Vector2.UP
	)

	## Damage from moving too fast
	add_movement_damage(motion)

	## Animations based on our current velocity
	add_animation(self.Velocity)

func _after_death():
	self.set_process(false)
	self.set_process_input(false)

	## Show the game over screen
	self.get_node("/root/Level1/OnscreenTexts/GameOver").set_visible(true)
