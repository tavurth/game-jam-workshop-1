extends Area2D

## Which model should we use for the character
export(PackedScene) var CharacterModel = null

onready var Player = get_node("/root/GlobalPlayer")

var Character = null
var AnimatedSpriteNode = null

func _ready():
	## We'll now instance the character model requested
	## Adding it to our node makes the character visible
	self.Character = CharacterModel.instance()
	
	## Which child of our charactir is the animated sprite
	self.AnimatedSpriteNode = Character.get_node("AnimatedSprite")
	
	self.add_child(Character)

func _on_CharacterSelector_mouse_entered():
	AnimatedSpriteNode.play("run")

func _on_CharacterSelector_mouse_exited():
	AnimatedSpriteNode.play("idle")

func _on_CharacterSelector_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton: return
	
	## Save the player model for later
	Player.set_model(CharacterModel)
	
	get_tree().change_scene("res://levels/Level1.tscn")