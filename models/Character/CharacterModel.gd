extends KinematicBody2D

func _take_damage(damage_amount):
	"""
	Called inside our self.Model instance
	"""
	## Usually we'll reference a character model from a
	## Node2D which contains our specific character info
	## Inside the character model we detect collisions and
	## animations, so when we take damage, it'll be here.
	## I'm simply now going to pass the damage to the parent
	self.get_parent()._take_damage(damage_amount)

func _add_velocity(velocity_amount):
	## Same as _take_damage
	## Here we may have a collision with the KinematicBody2D
	## But we're tracking health and velocity in the parent
	## We'll pass this event through
	self.get_parent()._add_velocity(velocity_amount)

func _damage_knockback(velocity_amount):
	self.get_parent()._damage_knockback(velocity_amount)
