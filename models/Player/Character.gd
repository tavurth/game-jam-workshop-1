extends Node2D

## Declared with var so we can change these
## from inherited scopes
var GRAVITY = 9.81 * 2
var MOVE_SPEED = 8
var FRICTION = 0.95
var JUMP_SPEED = 400
var CLAMP_SPEED = 200

const DEADZONE = 8

var Health = 3
var Velocity = null
var IsInAnimation = false

## Which will be used for our internal model
export(PackedScene) var CharacterModel = null

## Our internal reference to a character model
var Model

func _ready():
	self.Health = 3
	self.Velocity = Vector2()

	## Initialize our character model for this Character
	if CharacterModel:
		self.set_model(CharacterModel)

	set_physics_process(true)

func model_sprite():
	return Model.get_node("AnimatedSprite")

func set_model(character_model):
	"""
	Call to create our character model as a child of this scene
	We'll also reparent our child camera to the new model node
	as then the camera will automatically follow our player
	"""
	## Override any defaults we've set
	if self.CharacterModel:
		self.CharacterModel = character_model

	## Cleanup any previous model
	var previous_model = self.Model

	## Add the model to our main node
	self.Model = character_model.instance()
	self.add_child(self.Model)

	## Setup our animations callback
	self.model_sprite().connect("animation_finished", self, "_after_animation")

	if previous_model:
		previous_model.queue_free()

func is_alive():
	return Health > 0

func add_animation(motion):
	"""
	Use the players current velocity to update the characters
	animation type.
	"""
	## We're already playing something important
	if self.IsInAnimation:
		return

	## We're moving to the right
	if motion.x > DEADZONE:
		self.model_sprite().flip_h = false
		return self.model_sprite().play("run")

	## We're moving to the left
	if motion.x < -DEADZONE:
		self.model_sprite().flip_h = true
		return self.model_sprite().play("run")

	## We're not moving
	self.model_sprite().play("idle")

func can_jump(motion):
	if not is_alive(): return false

	## Don't allow the player to continuously jump when close to a wall
	## This would result in them shooting up in the air
	if motion.y < -100:
		return false

	return self.Model.is_on_floor() or self.Model.is_on_wall()

func add_gravity(motion):
	if self.Model.is_on_floor():
		return motion

	return motion + Vector2(0, GRAVITY)

func add_friction(motion, friction = FRICTION):
	## Only apply friction in the xDirection
	return motion * Vector2(friction, 1)

func clamp_movement(motion, maximum = CLAMP_SPEED):
	## No speed limit in yDirection
	motion.x = clamp(motion.x, -maximum, maximum)

	return motion

func add_movement_damage(motion):
	"""
	Adding damage when the player stops moving suddenly
	Such as when falling too far, or travelling horizontally too fast
	"""
	## Low speed does no damage
	if motion.length() < 3000: return

	## For every collisioning object we'll take one damage
	for i in self.Model.get_slide_count():
		self._take_damage(1)

func _take_damage(damage_amount):
	"""
	Generic damage taking function
	Will start to play damage animation, and if we're dead after
	taking damage, will transition into death processing
	"""
	self.Health -= damage_amount

	## Prevents us from adding motion assisted animations
	self.IsInAnimation = true

	## Character will now die
	if self.Health <= 0:
		self.model_sprite().play("die")

		## After we've finished dying, allow our class to run custom logic
		self.model_sprite().connect("animation_finished", self, "_after_death")

		## We want death to be our final animation
		self.model_sprite().disconnect("animation_finished", self, "_after_animation")
		return

	## We'll cleanup after the animation finishes
	self.model_sprite().play("hit")

func _add_velocity(velocity_amount):
	self.Velocity += velocity_amount

func _damage_knockback(velocity_amount):
	if not self.is_alive(): return
	self._add_velocity(velocity_amount)

func _after_death():
	self.set_process(false)
	self.set_process_input(false)

func _after_animation():
	## Allows us to once again select motion assisted animations
	self.IsInAnimation = false

func _physics_process(delta):
	"""
	Physics process is called once per physics frame
	and is the correct place to put any and all physics processing.

	Here we'll slowly build up our idea of what the characters
	next velocity should be, based on input and environmental factors.

	Finally we'll `move_and_slide` to determine the next velocity,
	and factor collisions automatically (godot)

	NOTE: This function should be extended when you extend this class
	"""
	pass
