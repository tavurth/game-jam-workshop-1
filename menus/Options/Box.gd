extends RigidBody2D

var is_held = false
const mouse_drag_scale = 1

func _ready():
	self.is_held = false
	set_process_input(true)

func _integrate_forces(state):
	## Godot default handling
	if not is_held:
		return state.set_linear_velocity(state.get_linear_velocity())

	var lv = state.get_linear_velocity()
	var mouse = get_global_mouse_position()

	state.set_linear_velocity((mouse - self.position) * mouse_drag_scale)

func _input(event):
	## Only mouse button events
	if not event is InputEventMouseButton:
		return

	if event.pressed: return
	if event.button_index != BUTTON_LEFT: return

	self.is_held = false


func _on_Box_input_event(viewport, event, shape_idx):
	## Only mouse button events
	if not event is InputEventMouseButton:
		return

	if not event.pressed: return
	if event.button_index != BUTTON_LEFT: return

	self.is_held = true
