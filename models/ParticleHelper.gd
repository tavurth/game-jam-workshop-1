extends CPUParticles2D

## Prevents us from incrementing the number of particles
## Every time we call the update_graphics function
## Because this value never changes we can always use it
## As a base for our particle multiplier calculation function
## (How many particles should we emit based on the global graphics config)
onready var amount_initial = self.amount

func _ready():
	add_to_group("particle_emitters")
