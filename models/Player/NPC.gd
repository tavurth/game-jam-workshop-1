extends "res://models/Player/Character.gd"

func _ready():
	set_physics_process(true)

func should_move():
	if not self.is_alive():
		return false

	## Don't start to move unless we're standing still
	if abs(self.Velocity.x) > self.DEADZONE:
		return false


	## 1% chance to start to move every frame standing still
	if randi() % 100 > 98:
		return true

func generate_new_direction():
	## 50% chance to move right
	if randi() % 10 >= 5:
		return Vector2(40, 0)

	## And 50% left
	return Vector2(-40, 0)

func get_initial():
	if should_move():
		return generate_new_direction();

	return self.Velocity

func _physics_process(delta):
	"""
	Physics process is called once per physics frame
	and is the correct place to put any and all physics processing.

	Here we'll slowly build up our idea of what the characters
	next velocity should be, based on input and environmental factors.

	Finally we'll `move_and_slide` to determine the next velocity,
	and factor collisions automatically (godot)
	"""
	var motion = get_initial()

	motion = add_gravity(motion)
	motion = add_friction(motion, .995)

	self.Velocity = self.Model.move_and_slide(
		## Limit maxium speed in any direction
		clamp_movement(motion),

		## Specify our UP vector
		Vector2.UP
	)

	add_movement_damage(motion)
	add_animation(self.Velocity)
