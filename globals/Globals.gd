extends Node2D

var FileLoader = preload("res://lib/file.gd").new()

const MAX_DETAIL = 3

var Configuration = {
	'LightDetail': 2,
	'TextureDetail': 2,
	'PhysicsDetail': 2,
	'ParticleDetail': 2,
}

func load_config():
	var config = FileLoader.load_json("options")
	for key in config:
		## Key does not already exist
		if not self.Configuration.get(key):
			continue

		## Save the configuration
		self.Configuration[key] = config[key]

func save_config():
	FileLoader.save_json("options", self.Configuration)

func set_configuration(name, value):
	self.Configuration[name] = value
	self.update_graphics()
	self.save_config()

func _ready():
	self.load_config()
	self.save_config()
	self.update_graphics()

func update_graphics():
	self.updated_detail_light()
	self.updated_detail_texture()
	self.updated_detail_physics()
	self.updated_detail_particle()

func set_shadow_size(size):
	ProjectSettings.set_setting("rendering/quality/shadow_atlas/size", size)
	ProjectSettings.set_setting("rendering/quality/directional_shadow/size", size)

func toggle_shadows(shadows_enable):
	for light in get_tree().get_nodes_in_group("shadow_casters"):
		light.shadow_enabled = shadows_enable

func updated_detail_light():
	toggle_shadows(self.Configuration.LightDetail > 0)

	## High resolution soft shadows
	if self.Configuration.LightDetail > 2:
		self.set_shadow_size(4096)
		ProjectSettings.set_setting("rendering/quality/shadows/filter_mode", Light2D.SHADOW_FILTER_PCF13)

	## Low resolution soft shadows
	elif self.Configuration.LightDetail > 1:
		self.set_shadow_size(2048)
		ProjectSettings.set_setting("rendering/quality/shadows/filter_mode", Light2D.SHADOW_FILTER_PCF3)

	else:
		self.set_shadow_size(0)
		ProjectSettings.set_setting("rendering/quality/shadows/filter_mode", Light2D.SHADOW_FILTER_NONE)


func updated_detail_physics():
	ProjectSettings.set_setting("physics/common/physics_fps", self.Configuration.PhysicsDetail * 20 + 10)

func set_particle_count(multiplier):
	"""
	Iterate over every particle emitter in our scene tree, setting the corrected amount
	based on our Global particle emitter count
	"""
	for particle in get_tree().get_nodes_in_group("particle_emitter"):
		## For less bugs we can attach a particle helper script
		## This ensures that if we call the graphics update more than one time
		## We won't add particles each time
		if particle.get("amount_initial"):
			particle.set_amount(particle.amount_initial * multiplier)

func updated_detail_particle():
	set_particle_count(self.Configuration.ParticleDetail + 1)

func updated_detail_texture():
	pass
