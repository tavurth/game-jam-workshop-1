extends Area2D

export(String) var uuid = ""

func _ready():
	if GlobalPlayer.has_collected_star(self.uuid):
		self.queue_free()

func _on_Star_body_entered(body):
	self.get_node("AnimationPlayer").play("Explode")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name != "Explode": return

	## Remove the star from the world
	self.queue_free()

	## Update the total stars we've collected (Global player state)
	GlobalPlayer.collect_star(self.uuid)
