extends Node2D

var counter = 0
var last_pause_result

func _ready():
	set_process(true)

func get_distance():
	return self.position.distance_to(GlobalPlayer.Character.Model.position)

func should_pause():
	## We don't need to check every frame
	## Here we'll check once every 10 frame
	if counter < 10:
		counter += 1
		return last_pause_result
	counter = 0

	last_pause_result = get_distance() > 3000
	return last_pause_result

func run_pause_check():
	if should_pause():
		self.set_pause_mode(PAUSE_MODE_STOP)

	else:
		self.set_pause_mode(PAUSE_MODE_PROCESS)

func _process(delta):
	pass
    #run_pause_check()
