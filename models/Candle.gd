extends Light2D

export(String, DIR) var TextureDirectory = null

func select_texture():
	## Load the desired detail texture
	self.set_texture(load(str(TextureDirectory, '/', Globals.Configuration.TextureDetail, '.png')))

	## Scale the light by the correct amount
	## Lower details have as smaller texture so we need to scale the size in game
	var texture_detail = pow(2, Globals.MAX_DETAIL - Globals.Configuration.TextureDetail)
	self.set_texture_scale(float(texture_detail) * .4)

	## Disable the secondary light animation if low graphics mode
	if Globals.Configuration.ParticleDetail < 2:
		if self.get_node('Light'):
			self.get_node('Light').queue_free()

func _ready():
	select_texture()
