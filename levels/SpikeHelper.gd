extends Area2D

onready var Spikeball = preload('res://models/Spikeball.gd')

func _ready():
	pass

func _on_SpikeHelper_body_entered(body):
	if not body is Spikeball:
		return

	body.shouldReset = true
