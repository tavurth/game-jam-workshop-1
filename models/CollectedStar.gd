extends Node2D

export(int) var StarIndex = 0

func _ready():
	## Setup our starting state
	_collected_star(GlobalPlayer.SAVE_STATE.StarsCollected)

	## Attach new stars collected state
	GlobalPlayer.connect("collected_star", self, "_collected_star")

func _collected_star(stars_collected):
	if len(stars_collected) > self.StarIndex:
		self.get_node("AnimationPlayer").play("Appear")
