extends Node2D

const PlayerScene = preload("res://models/Player/Player.tscn")

signal collected_star

## Create an instance of the player node
onready var Character = PlayerScene.instance()

var FileLoader = preload("res://lib/file.gd").new()

const INITIAL_SAVE_STATE = {
	"StarsCollected": []
}

func dict_shallowcopy(source):
	"""
	Allows us to clone a dict (only the toplevel items)
	"""
	var to_return = {}
	for key in source:
		to_return[key] = source[key]

	return to_return

var SAVE_STATE = dict_shallowcopy(INITIAL_SAVE_STATE)

func unique(array):
	var to_return = {}

	for item in array:
		to_return[item] = true

	return to_return.keys()

func load_config():
	var config = FileLoader.load_json("player")
	for key in config:
		## Key does not already exist
		if not self.SAVE_STATE.has(key):
			continue

		## Save the configuration
		self.SAVE_STATE[key] = config[key]

	## Make sure we don't duplicate stars in the save file for some reason
	self.SAVE_STATE.StarsCollected = unique(self.SAVE_STATE.StarsCollected)


	## Set our initial star state
	emit_signal("collected_star", self.SAVE_STATE.StarsCollected)

func reset_save_state():
	self.SAVE_STATE = dict_shallowcopy(INITIAL_SAVE_STATE)
	self.save_config()

	emit_signal("collected_star", self.SAVE_STATE.StarsCollected)

func save_config():
	FileLoader.save_json("player", SAVE_STATE)

func _ready():
	load_config()

	## Save the config (generate new if not have)
	save_config()

	set_process_input(true)

func has_collected_star(uuid):
	return self.SAVE_STATE.StarsCollected.has(uuid)

func reset():
	self.Character = PlayerScene.instance()

func set_model(character_model):
	## Set the character model there
	Character.set_model(character_model)

func collect_star(uuid):
	if has_collected_star(uuid):
		return

	self.SAVE_STATE.StarsCollected.append(uuid)

	emit_signal("collected_star", self.SAVE_STATE.StarsCollected)

	## Save the star collection state
	save_config()

func _input(event):
	if not event is InputEventKey: return

	## Allow the user to return to character selection by pressing escape
	if Input.is_action_pressed("ui_cancel"):
		GlobalPlayer.reset()
		get_tree().change_scene("res://menus/CharacterSelection/CharacterSelection.tscn")

