func save(file_name, content):
	var file = File.new()
	file.open(str("user://", file_name, ".dat"), File.WRITE)
	file.store_string(content)
	file.close()

func load(file_name):
	var file = File.new()
	file.open(str("user://", file_name, ".dat"), File.READ)
	var content = file.get_as_text()
	file.close()
	return content

func load_json(file_name):
	var to_return = parse_json(self.load(file_name))
	return to_return if to_return else {}

func save_json(file_name, data):
	self.save(file_name, JSON.print(data))

