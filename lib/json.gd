
func load(name):
	var file = File.new()
	file.open(str("res://", name, ".json"), file.READ)
	var to_return = parse_json(file.get_as_text())
	file.close()

	return to_return
