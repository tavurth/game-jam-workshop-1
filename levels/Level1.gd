extends Node2D

func _ready():
	## Load our graphics settings
	Globals.update_graphics()

	## Make sure we've completely reset the player
	GlobalPlayer.reset()

	## Move the character to the center of the screen
	GlobalPlayer.Character.position = Vector2(0, 470)

	## Now add him to our scene
	self.add_child(GlobalPlayer.Character)
