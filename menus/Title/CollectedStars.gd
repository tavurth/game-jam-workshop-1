extends Label

func _ready():
	text = ''

	for star in GlobalPlayer.SAVE_STATE.StarsCollected:
		if not len(star): continue

		text += star
		text += '\n'
