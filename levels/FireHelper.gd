extends Area2D

func _ready():
	pass

func fire_damage(body):
	if body.has_method("_take_damage"):
		body._take_damage(.8)

	## Make the actor jump up from fire damage
	if body.has_method("_damage_knockback"):
		body._damage_knockback(Vector2(0, -1400))

func _on_FireHelper_body_entered(body):
	fire_damage(body)

func _on_Timer_timeout():
	for body in get_overlapping_bodies():
		fire_damage(body)
