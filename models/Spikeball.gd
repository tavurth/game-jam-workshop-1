extends RigidBody2D

var initialPosition = Vector2(0, 0)
var shouldReset = false

func _ready():
	initialPosition = self.position

func _integrate_forces(state):
	## Only when we should reset to starting position
	if not shouldReset: return

	shouldReset = false
	state.transform = Transform2D(0.0, initialPosition)

func _on_Fireball_body_entered(body):
	if body.has_method("_take_damage"):
		body._take_damage(10)

		## Delete after damage?
		# self.queue_free()
